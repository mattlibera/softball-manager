<?php

// HOME
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push("Home", route('home'));
});

// ACCOUNT
Breadcrumbs::register('account', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Account", route('account'));
});
Breadcrumbs::register('profile.show', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("My Profile", route('profile.show'));
});
Breadcrumbs::register('profile.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Edit Profile", route('profile.edit'));
});
Breadcrumbs::register('account.settings', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Account Settings", route('account.settings'));
});

// ADMIN
Breadcrumbs::register('admin', function ($breadcrumbs) {
    $breadcrumbs->push("Admin", route('admin'));
});

// Package breadcrumbs
foreach(config('ccps.modules') as $name => $module) {
    require(base_path('vendor/' . $module['package'] . '/src/breadcrumbs/' . $name . '.php'));
}