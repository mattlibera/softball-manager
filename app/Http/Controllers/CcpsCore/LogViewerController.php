<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\LogViewerController as BaseController;

class LogViewerController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
