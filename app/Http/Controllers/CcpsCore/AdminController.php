<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\AdminController as BaseController;

class AdminController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
