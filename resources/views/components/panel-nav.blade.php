<a href="{{ $url }}">
    <div class="w-{{ $span or '100' }} p-3">
        <div class="card">
            <div class="card-body text-center">
                <i class="fas fa-5x fa-{{ $fa }} mb-3"></i><br>
                {{ $title }}
            </div>
        </div>
    </div>
</a>