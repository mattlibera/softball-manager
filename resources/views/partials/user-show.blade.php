@if($context == 'profile')
    <h1>My Profile</h1>
@else
    <h1>User - {{ $userToShow->email }}</h1>
@endif

<div class="row mt-3">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h5>User Information</h5>
            </div>
            <div class="card-body">
                <img class="rounded-circle d-block mx-auto"
                     src="{{ $userToShow->avatar_url or asset('images/default-avatar.jpg')}}">
                <p><strong>Name</strong>: {{ $userToShow->fullname }}</p>
                <p><strong>Provider</strong>: <i class="{{ $userToShow->provider_icon or 'fas fa-address-book' }} mr-1"
                                                 title="Provider: {{ ucwords($userToShow->provider) }}"></i> {{ ucwords($userToShow->provider) }}
                </p>
                @if($userToShow->provider != 'local')
                    <p><strong>Provider ID</strong>: {{ $userToShow->id_from_provider }}</p>
                @endif
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card mb-3">
            <div class="card-header">
                <h5>Access Information</h5>
            </div>
            <div class="card-body">
                <p><strong>Last login</strong>: {{ $userToShow->last_login }}</p>

                <p>
                    <strong>Role(s) granted</strong>:
                    @if(count($userToShow->roles) > 0)
                        <ul>
                            @foreach ($userToShow->roles as $thisRole)
                                <li><a href="{{ route('role.show', ['userToShow' =>  $thisRole->id]) }}">{{ $thisRole->display_name }}</a></li>
                            @endforeach
                        </ul>
                    @else
                        None
                    @endif
                </p>

                <p><strong>API Key</strong>: {{ $userToShow->api_key or 'not set' }}</p>
            </div>
        </div>

        @include('partials.audit-history-card', ['modelType' => 'user', 'model' => $userToShow])
    </div>

</div>

<hr>

<div class="row">
    <div class="col">
        <h3>Actions</h3>

        <div class="d-flex flex-column flex-md-row">
            @if($context == 'profile')
                <a href="{{ route('profile.edit') }}" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</a>
            @else
                @permission('users.edit')
                <a href="{{ route('user.edit', ['userToEdit' => $userToShow->id]) }}" class="btn btn-sm btn-warning mr-3 mb-3"><i class="fas fa-edit"></i> Edit</a>

                @if(!empty($userToShow->api_key))
                    <form method="POST" action="{{ route('user.apikey.clear', ['user' => $userToShow->id]) }}"
                          onsubmit="return confirm('This will invalidate the user\'s API Key. Proceed?')">

                        <button type="submit" class="btn btn-sm btn-primary mr-3 mb-3"><i class="fas fa-code"></i> Clear API Key</button>

                        {{ csrf_field() }}
                    </form>
                @endif
                @endpermission

                @permission('users.delete')
                <form method="POST" action="{{ route('user.destroy', ['user' => $userToShow->id]) }}"
                      onsubmit="return confirm('Do you really want to delete this user?')" class="mb-3">

                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Delete</button>

                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                </form>
                @endpermission
            @endif
        </div>
    </div>
</div>