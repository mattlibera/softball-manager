@if(count($audits) > 0)
    @foreach($audits as $audit)
    <div class="audit_event mb-3">
        <h3>Audit #{{ $audit->id }}</h3>
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Audit Details</h5>
                    </div>
                    <div class="card-body">
                        <p><strong>Event</strong>: {{ $audit->event }}</p>
                        <p><strong>Date/Time</strong>: {{ $audit->created_at}}</p>
                        @if($audit->event == 'updated')
                        <div class="row">
                            <div class="col">
                                <p><strong>Old Values:</strong></p>
                                <small>
                                    <ul>
                                        @foreach($audit->old_values as $key => $value)
                                            <li><code>{{ $key }}</code>: {{ is_array($value) ? json_encode($value) : $value }}</li>
                                        @endforeach
                                    </ul>
                                </small>
                            </div>
                            <div class="col">
                                <p><strong>New Values:</strong></p>
                                <small>
                                    <ul>
                                        @foreach($audit->new_values as $key => $value)
                                            <li><code>{{ $key }}</code>: {{ is_array($value) ? json_encode($value) : $value}}</li>
                                        @endforeach
                                    </ul>
                                </small>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">User Details</h5>
                    </div>
                    <div class="card-body">
                        <p><strong>User</strong>: <a href="{{ route('user.show', ['userToShow' => $audit->user->id]) }}">{{ $audit->user->fullname }}</a></p>
                        <p><strong>IP Address</strong>: <a href="http://www.ip-tracker.org/locator/ip-lookup.php?ip={{ $audit->ip_address }}" target="_blank">{{ $audit->ip_address }}</a></p>
                        <p><strong>User Agent</strong>: {{ $audit->user_agent }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@else
    <p>No audits found.</p>
@endif